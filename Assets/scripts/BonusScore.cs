﻿
using UnityEngine;


public class BonusScore : MonoBehaviour
{
    public GameObject PointScore;
    public Transform target;
    public float speed;
    private int a,b;
 
    public AudioClip hit;
   


    // Use this for initialization
    void Start()
    {
        PointScore = GameObject.FindGameObjectWithTag("GameController");
        

    }

    // Update is called once per frame
    void Update()
    {
        if(PointScore.GetComponent<GameCntrl>().lose==true)
			{
				GetComponent<Rigidbody>().useGravity=false;
			}
        transform.position = Vector3.MoveTowards(transform.position, target.position, Time.deltaTime * speed);
        if(transform.position.z < -2.5f)
        {
        PointScore.GetComponent<GameCntrl>().Discount(1);
      Destroy(gameObject);
         }
    }
      void OnMouseDown()
    { 
        PointScore.GetComponent<GameCntrl>().IncreaseScore(2);  
        Destroy(gameObject);
      if(PlayerPrefs.GetInt("Sound") != 1)
        {
         AudioSource.PlayClipAtPoint(hit, transform.position);
         }
         
       
    }
    
    }

