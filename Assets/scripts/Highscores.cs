﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class Highscores : MonoBehaviour {

const string privateCode = "s0MbxtZN2UuWND4FljgXbgWcR-TrPLr0qLpzYVRHkfHg";
	const string publicCode = "5a735a6339992b09e46a1ba5";
	const string webURL = "http://dreamlo.com/lb/";
	
	DisplayHighscores highscoreDisplay;
	public Highscore[] highscoresList;
	static Highscores instance;
	 public InputField inputF;
	 public string Name;
	private int plus;
	public GameObject checkName, backToMenu;
	public Text uniqueText;
	public GameObject cntrl;
	
	


	void Awake() {
	
		highscoreDisplay = GetComponent<DisplayHighscores> ();
		instance = this;
		checkName.SetActive(true);
		backToMenu.SetActive(false);
		}

	public static void AddNewHighscore(string username, int score) {
		
		instance.StartCoroutine(instance.UploadNewHighscore(username,score));
	}
	void Update()
	{
			
	}

	public void Compare(string NewName)
	 {	plus=0;		
		 for (int i = 0; i <highscoresList.Length; i ++)
		 	{
		if(highscoresList[i].username.CompareTo(NewName)==0)
				{		
				plus++;		                                                                                          
				}
		 }

	
	}
	IEnumerator UploadNewHighscore(string username, int score) {
		WWW www = new WWW(webURL + privateCode + "/add/" + WWW.EscapeURL(username) + "/" + score);
		yield return www;

		if (string.IsNullOrEmpty(www.error)) {
			print ("Upload Successful");
			DownloadHighscores();
		}
		else {
			print ("Error uploading: " + www.error);
		}
	}

 
	public void DownloadHighscores() {
		StartCoroutine("DownloadHighscoresFromDatabase");
	}

	IEnumerator DownloadHighscoresFromDatabase() {
		WWW www = new WWW(webURL + publicCode + "/pipe/");
		yield return www;
		
		if (string.IsNullOrEmpty (www.error)) {
			FormatHighscores (www.text);
			highscoreDisplay.OnHighscoresDownloaded(highscoresList);
		}
		else {
			print ("Error Downloading: " + www.error);
		}
	}

	void FormatHighscores(string textStream) {
		string[] entries = textStream.Split(new char[] {'\n'}, System.StringSplitOptions.RemoveEmptyEntries);
		highscoresList = new Highscore[entries.Length];

		for (int i = 0; i <entries.Length; i ++) {
			string[] entryInfo = entries[i].Split(new char[] {'|'});
			string username = entryInfo[0];
			int score = int.Parse(entryInfo[1]);
			highscoresList[i] = new Highscore(username,score);
		//	print (highscoresList[i].username + ": " + highscoresList[i].score);
		}
	}

	public void CheckName()
	{
		PlayerPrefs.DeleteKey("Name");
		PlayerPrefs.SetString("Name", inputF.textComponent.text);
		Compare(PlayerPrefs.GetString("Name"));
		if(plus==0)
			{
				if(PlayerPrefs.GetString("Name").Length>0)
				{
				PlayerPrefs.SetInt("Online", 1);		 
				checkName.SetActive(false);
				backToMenu.SetActive(true);
				}
			}
		else if(plus!=0)
		{
			PlayerPrefs.SetInt("Online", 0);
			uniqueText.text="No Try again";
		}

	}
			

}
	

	

public struct Highscore {
	public string username;
	public int score;

	public Highscore(string _username, int _score) {
		username = _username;
		score = _score;
	}
}
