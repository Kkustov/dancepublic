﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusLives : MonoBehaviour {
	public GameObject PointScore;
	public Rigidbody BonusLivesRigib;	
	public Vector3 force;

	void Start () {
		PointScore = GameObject.FindGameObjectWithTag("GameController");
		BonusLivesRigib = GetComponent<Rigidbody>();
		
	}
	
	// Update is called once per frame
	void Update () {
			if(PointScore.GetComponent<GameCntrl>().lose==true)
			{
				 GetComponent<Rigidbody>().useGravity=false;
			}
			BonusLivesRigib = GetComponent<Rigidbody>();		
			BonusLivesRigib.AddForce(force);		
			force = new Vector3 (Random.Range(-5f,5f),Random.Range(-5f,5f),0);
	}
	void OnMouseDown(){
		PointScore.GetComponent<GameCntrl>().PlusLive();
		Destroy(GameObject.Find("PlusLive(Clone)"));
				
	}
}
