﻿//using System;

using UnityEngine;
using UnityEngine.UI;
using System.Collections;



public class GameCntrl : MonoBehaviour {

    public bool lose, SoundBonusB,BonusLive, comboB;

	public Text coinsScore;
	public int score, endscore;
	public Text scoreText;
	public Text liveText;
	private int coins;
	public GameObject timeUp;
	public GameObject timetrue;
	public int lives;
	public GameObject Light;
	public AudioSource gameMusic;
	public AudioSource loseMusic;
	public GameObject BonusSound, SpauwnerMove, BonusLiv, BonusCombo;
	int a;
	public string username;
	GameObject highscoresManager;
	public Vector3 randomPosition;
	public int randomPerCent;
	public float timerBonus;
	
	void Start () {      
		highscoresManager = GameObject.FindGameObjectWithTag("score");
		if(PlayerPrefs.GetInt("Music")==1){ gameMusic.mute = true;}
		else{gameMusic.mute = false;}
		if(PlayerPrefs.GetInt("Light")==1){ Light.SetActive(false);}
		else{Light.SetActive(true);}
        lose = false;
		PlayerPrefs.SetFloat("dist", 2.5f);
		var bonusSo = BonusSound.GetComponent<Rigidbody>();
		bonusSo.useGravity=true;		
		StartCoroutine(TimeToBonus());
	}

	
	void Update () {				
		if(SoundBonusB==true)
			{
			BonusS();
			}
		else if(BonusLive==true)
			{
			BonusL();
			}
		else if(comboB==true)
		{
			timerBonus = 10 + (2*(PlayerPrefs.GetInt("levelComboBonus")));
			ComboBonus();
		}

		if(timerBonus>0)
		{
			timerBonus -= Time.deltaTime;
		}		
		

		if(randomPerCent>=(100-(15*PlayerPrefs.GetInt("levelSound")))&&
		(PlayerPrefs.GetFloat("dist")<=2f)&&((GameObject.Find("MusDown(Clone)")==null))&&
		(PlayerPrefs.GetInt("levelSound")>1))
		{
			SoundBonusB =true;			
		}
		else{SoundBonusB=false;}

		if((randomPerCent>=(100-PlayerPrefs.GetInt("levelGiveLives"))&&
		((GameObject.Find("PlusLive(Clone)")==null))&&
		(PlayerPrefs.GetInt("levelGiveLives")>=1)))
		{
			BonusLive =true;			
		}
		else{BonusLive=false;}

		
	
	if(Input.GetKey(KeyCode.Escape)){BackButtons();}

	
	}
	public void BonusL()
	{
		GameObject ob=Instantiate(BonusLiv,randomPosition, Quaternion.identity) as GameObject;
		BonusLive=false;
	}
 public void BonusS()
	{
	GameObject ob=Instantiate(BonusSound,randomPosition,Quaternion.identity) as GameObject;
	SoundBonusB=false;
	}
	public void ComboBonus()
	{
		GameObject ob=Instantiate(BonusCombo,randomPosition,Quaternion.identity) as GameObject;
		comboB=false;
	}
	public void restartButtons () {
        Application.LoadLevel("GAme");
		
	}
	public void BackButtons (){
		Application.LoadLevel("Menu");
		Destroy(highscoresManager);

	}
	public void PlusLive(){
		lives++;
		liveText.text = lives + "";
	}
		public void Discount(int dis)
	{
		lives -= dis;
		liveText.text = lives + "";
		if(lives ==0)
		{
			gameMusic.mute =true;
			if(PlayerPrefs.GetInt("Music")==0)
			{
				loseMusic.Play();
			}
		timetrue.SetActive(false);
		StartCoroutine(GoToLose());
		}
	}

	IEnumerator TimeToBonus(){
		yield return new WaitForSeconds(1f);
		if(lose==false)
		{
		randomPerCent = Random.Range(0,100);
		StartCoroutine(TimeToBonus());
		}
	}

		IEnumerator GoToLose(){
	yield return new WaitForSeconds(1.5f);
	Lose();}

	public void IncreaseScore(int increase)
	{
		score += increase;
		scoreText.text =  score + "";
		if((score %5==0)||(score%5==1f)){
			gameMusic.pitch += 0.005f;
			PlayerPrefs.SetFloat("dist", PlayerPrefs.GetFloat("dist")-0.05f);
			randomPosition = new Vector3 (Random.Range(-2.68f,2.81f),Random.Range(2.96f,-4.51f),-2f);
			
			}
		
	}

	public void Lose()
	{ 	timeUp.SetActive(true);
		lose =true;
		if	(

			PlayerPrefs.GetInt("Score")< score){
			PlayerPrefs.SetInt("Score", score);}
			coins = score/10;
			coinsScore.text = coins + "";			
			a = PlayerPrefs.GetInt("coins");
			a+= coins; 			
			PlayerPrefs.SetInt("coins", a);
			Highscores.AddNewHighscore(PlayerPrefs.GetString("Name"),PlayerPrefs.GetInt("Score"));
			
	}
	 
	public void EkranButton(){
		
			Lose();}
	public void EndButton(){
				Lose();
			}
	public void SoundDown(){		
			gameMusic.pitch -= (0.01f* PlayerPrefs.GetInt("levelSound"));
			PlayerPrefs.SetFloat("dist", PlayerPrefs.GetFloat("dist")+((0.1f* PlayerPrefs.GetInt("levelSound"))));
			
			
		}
	
}

	

