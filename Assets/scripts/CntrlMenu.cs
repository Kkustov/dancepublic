﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;



public  class CntrlMenu : MonoBehaviour {
 public GameObject menu1,menu2, menu3, chooseLang, Leaderbord, chooseName;
public GameObject settings;
public GameObject shop;
public GameObject mus, li, so;
public GameObject Light;
public bool m1=true,shopp=false, seti = false, laedBool = false, chooseNameBool=false;
public GameObject[] rus , eng;
public GameObject HighScore, DANCE;
public Text nameText, priseMusDownText,priseGiveLiveText, coinText1, coinText2;
public int priseMusDown, priseGiveLive;
 


public GameObject cntrl;


   void Start()
   {	// PlayerPrefs.DeleteKey("levelGiveLives");	
	   //PlayerPrefs.DeleteKey("levelSound");
	    if(PlayerPrefs.GetInt("levelSound")==0){PlayerPrefs.SetInt("levelSound",1);}
   //PlayerPrefs.DeleteAll();
	  	coinText1.text= PlayerPrefs.GetInt("coins").ToString();
   		 chooseName.SetActive(false);
	/*   if(PlayerPrefs.GetInt("First")==0){
		   menu1.SetActive(false);
		   chooseName.SetActive(true);
		   chooseNameBool = true;
		   m1=false;
	   }*/
	   Leaderbord.SetActive(false); 

		if(PlayerPrefs.GetInt("Online")==0)
		{
			DANCE.SetActive(false);
			ChooseName();
		}
	   
		  
	   
	   else {
		   nameText.text=PlayerPrefs.GetString("Name");
			menu1.SetActive(true); 
	   		chooseLang.SetActive(false);
			 chooseName.SetActive(false);

			   }
	    if(PlayerPrefs.GetInt("Sound")== 1){so.SetActive(false);
   		}
		else {so.SetActive(true);}

	   if(PlayerPrefs.GetInt("Light")== 1){li.SetActive(false);
   											Light.SetActive(false);
											}
		else {li.SetActive(true);
		Light.SetActive(true);
		}

		if(PlayerPrefs.GetInt("Music")== 1){mus.SetActive(false);}
		else {mus.SetActive(true);
		gameObject.GetComponent<AudioSource>().Play();}
	   
	   settings.SetActive(false);
	   shop.SetActive(false);
	 

	   menu2.SetActive(false);
	   menu3.SetActive(false); 	 
	   Highscores.AddNewHighscore(PlayerPrefs.GetString("Name"),PlayerPrefs.GetInt("Score"));
	  
   }

	
void Update () {
		
		DontDestroyOnLoad(HighScore);
		rus = GameObject.FindGameObjectsWithTag("Rus");	
		eng = GameObject.FindGameObjectsWithTag("Eng");
		if((Input.GetKey(KeyCode.Escape))&&(m1==true)){Application.Quit();}
		if((Input.GetKey(KeyCode.Escape))&&(laedBool==true)){LeaderbordBttnBack();}
		if((Input.GetKey(KeyCode.Escape))&&(seti==true)){Backsettingsbutton();}
		if((Input.GetKey(KeyCode.Escape))&&(shopp==true)){backShopButton();}
		if((Input.GetKey(KeyCode.Escape))&&(chooseNameBool==true)){ChooseNameBack();}
		if(PlayerPrefs.GetInt("ChooseL")==1){
			for(int i=0; i < rus.Length; i++){
		 	 	rus[i].SetActive(false);
				eng[i].SetActive(true);}}
		if(PlayerPrefs.GetInt("ChooseL")==2){
			for(int i=0; i < eng.Length; i++){
			 	 eng[i].SetActive(false);
				  rus[i].SetActive(true);}}

		
		if(PlayerPrefs.GetInt("Online") ==1)
		{
			StartCoroutine( GoNext());
		}
		
		
	}

	public void Language(){
		menu1.SetActive(false);
		chooseLang.SetActive(true);
	}
	public void ChooseEnglish(){
		menu1.SetActive(true);
		chooseLang.SetActive(false);
		PlayerPrefs.SetInt("ChooseL", 1);
	}
	public void ChooseRus(){
		menu1.SetActive(true);
		chooseLang.SetActive(false);
		PlayerPrefs.SetInt("ChooseL", 2);}

	public void SettingButton(){

		m1=false;
		seti=true;
		menu1.SetActive(false);
		settings.SetActive(true);


	}
	public void Playinfo1()
	{
		
		if(PlayerPrefs.GetInt("Reset")==1)
			{
			gameObject.GetComponent<AudioSource>().Stop();
			Application.LoadLevel("GAme");
			}
		else{
		
		menu1.SetActive(false);
		menu2.SetActive(true);
		}
	}
	public void Playinfo2(){
		
		 
		menu2.SetActive(false);
		menu3.SetActive(true);
	}
	public	void  PlayButton() {
				PlayerPrefs.SetInt("Reset", 1);
		gameObject.GetComponent<AudioSource>().Stop();
		Application.LoadLevel("GAme");}	
	
	public void Backsettingsbutton(){
		seti=false;
		m1=true;
		settings.SetActive(false);
		menu1.SetActive(true);}

	public void ShopButton(){
		coinText2.text = PlayerPrefs.GetInt("coins").ToString();
		if(PlayerPrefs.GetInt("levelSound")<7){
		priseMusDownText.text = priseMusDown.ToString(); 
		}
		else if(PlayerPrefs.GetInt("levelSound")>=7){
				priseMusDownText.text = "Too Slow...";
			}
		if(PlayerPrefs.GetInt("levelGiveLives")<7)
		{
			priseGiveLiveText.text = priseGiveLive.ToString();
		}
		else if(PlayerPrefs.GetInt("levelGiveLives")>=7)
		{
			priseGiveLiveText.text = "Too many lives...";
		}
		gameObject.GetComponent<AudioSource>().Stop();

		menu1.SetActive(false);
		shop.SetActive(true);
			Light.SetActive(false);
			li.SetActive(false);
			m1=false;
			shopp=true;
			}
	public void backShopButton()
	{
		m1=true;
		shopp=false;
		menu1.SetActive(true);
		shop.SetActive(false);
		if(PlayerPrefs.GetInt("Music")== 0)
			{
			mus.SetActive(true);
			gameObject.GetComponent<AudioSource>().Play();
			}
		if(PlayerPrefs.GetInt("Light")== 0)
			{
			Light.SetActive(true);
			li.SetActive(true);
			}
	}
	public void MusicButton(){
		if(PlayerPrefs.GetInt("Music") != 1){
			PlayerPrefs.SetInt("Music", 1);
			mus.SetActive(false);
			gameObject.GetComponent<AudioSource>().Stop();
			} else{PlayerPrefs.SetInt("Music", 0);
			mus.SetActive(true);
			gameObject.GetComponent<AudioSource>().Play();
			}

	}
	public void ExitButton(){
		Application.Quit();
	}
	
	public void infoButton()
		{
		menu1.SetActive(false);
		menu2.SetActive(true);
	}
	/*
	public void NextinfoButton(){
		info1.SetActive(false);
		info2.SetActive(true);
	}
	public void BackinfoButton(){
		info2.SetActive(false);
		menu1.SetActive(true);
	}
	*/
	public void LightButton(){
			if(PlayerPrefs.GetInt("Light") != 1){
			PlayerPrefs.SetInt("Light", 1);
			Light.SetActive(false);
			li.SetActive(false);
			} else{PlayerPrefs.SetInt("Light", 0);
			Light.SetActive(true);
			li.SetActive(true);}
	}
	public void SoundButton(){
		if(PlayerPrefs.GetInt("Sound") != 1){
			PlayerPrefs.SetInt("Sound", 1);		
			so.SetActive(false);
			} else{PlayerPrefs.SetInt("Sound", 0);			
			so.SetActive(true);
	}
}
	public void LeaderbordBttn()
	{
		menu1.SetActive(false);
		Leaderbord.SetActive(true);
		m1=false;
		laedBool=true;
	}
	public void LeaderbordBttnBack()
	{
		menu1.SetActive(true);
		Leaderbord.SetActive(false);
		m1=true;
		laedBool=false;
	}
	public void ChooseName(){
			menu1.SetActive(false);
		   chooseName.SetActive(true);
		   chooseNameBool = true;
		   m1=false;
		 
	}
	public void ChooseNameBack()
	{
		DANCE.SetActive(true);
		chooseName.SetActive(false);
		chooseNameBool = false;		
		nameText.text=PlayerPrefs.GetString("Name");
		PlayerPrefs.SetInt("Online", 2);		
		if(PlayerPrefs.GetInt("ChooseL")==0)Language();
		else if(PlayerPrefs.GetInt("ChooseL")!=0) menu1.SetActive(true);
		/* 
		menu1.SetActive(true);
		chooseName.SetActive(false);
		chooseNameBool = false;
		m1=true;		 
		*/
	}
	public void GoOfflineButtn()
	{
		DANCE.SetActive(true);
		chooseName.SetActive(false);
		chooseNameBool = false;
		PlayerPrefs.SetInt("Online", 0);
		if(PlayerPrefs.GetInt("ChooseL")==0)Language();
		else if(PlayerPrefs.GetInt("ChooseL")!=0) menu1.SetActive(true);
		}

	IEnumerator GoNext(){
	yield return new WaitForSeconds(1);
	ChooseNameBack();
		}
	public void MusDownBttn()
	{
		if(PlayerPrefs.GetInt("levelSound")<7)
		{
			if((PlayerPrefs.GetInt("coins"))>=priseMusDown)
			{
			int coins = PlayerPrefs.GetInt("coins")-priseMusDown;
			PlayerPrefs.SetInt("coins", coins);
			coinText2.text = PlayerPrefs.GetInt("coins").ToString();
			priseMusDown= (10*PlayerPrefs.GetInt("levelSound"));
			priseMusDownText.text = priseMusDown.ToString(); 
			PlayerPrefs.SetInt("levelSound", PlayerPrefs.GetInt("levelSound")+1);
			}
		}
			else if(PlayerPrefs.GetInt("levelSound")>=7)
			{

				priseMusDownText.text = "Too Slow...";
			}
		
		
	}

	public void GiveLiveBttn()
	{
		if(PlayerPrefs.GetInt("levelGiveLives")<7)
		{
			if((PlayerPrefs.GetInt("coins"))>=priseGiveLive)
			{	
				int coins = PlayerPrefs.GetInt("coins")-priseGiveLive;
				PlayerPrefs.SetInt("coins", coins);
				coinText2.text = PlayerPrefs.GetInt("coins").ToString();
				priseGiveLive =20+ (10*PlayerPrefs.GetInt("levelGiveLives"));
				priseGiveLiveText.text = priseGiveLive.ToString();
				PlayerPrefs.SetInt("levelGiveLives", PlayerPrefs.GetInt("levelGiveLives")+1);		

			}
		}
		else if(PlayerPrefs.GetInt("levelGiveLives")>=7)
		{
			priseGiveLiveText.text = "Too many lives...";
		}
	}


	
}