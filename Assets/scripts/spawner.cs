﻿using UnityEngine;
using System.Collections;

public class spawner : MonoBehaviour
{

    public GameObject[] ObjectsToSpwan;
    public float MinDelay;
    public float MaxDelay;
     public int score;
     public Vector3 randomPosition;

    void Start()
    {   
        StartCoroutine(Spawner());
    }

    void Update ()
    {    randomPosition= new Vector3 (Random.Range(-2.68f,2.81f),Random.Range(2.96f,-4.51f),1f);
         score= GameObject.FindGameObjectWithTag("GameController").GetComponent<GameCntrl>().score;
        if(PlayerPrefs.GetFloat("dist")>2.5f){MaxDelay=1f;MinDelay=0.3f;}
        if((PlayerPrefs.GetFloat("dist")<2.5f)&&(PlayerPrefs.GetFloat("dist")>2.3f)){MaxDelay=0.8f;MinDelay=0.3f;}
        if((PlayerPrefs.GetFloat("dist")<2.3f)&&(PlayerPrefs.GetFloat("dist")>2f)){MaxDelay=0.6f;MinDelay=0.2f;}
        if((PlayerPrefs.GetFloat("dist")<2f)&&(PlayerPrefs.GetFloat("dist")>1.5f)){MaxDelay=0.5f;MinDelay=0.2f;}
        if((PlayerPrefs.GetFloat("dist")<1.5f)){MaxDelay=0.4f;MinDelay=0.05f;}
    }
    IEnumerator Spawner()
    {
        yield return new WaitForSeconds(Random.Range(MinDelay, MaxDelay));
        Spawn();
    }

    void Spawn()
    {
        GameObject obj = Instantiate(ObjectsToSpwan[Random.Range(0, ObjectsToSpwan.Length)], randomPosition, transform.rotation) as GameObject;
        StartCoroutine(Spawner());

    }
}
