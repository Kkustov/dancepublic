﻿
using UnityEngine;

public class moveball : MonoBehaviour {
    public Vector2 Force;
    public Rigidbody2D cube;
	// Use this for initialization
	void Start () {
        cube = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        cube.AddForce(Force);
        }
   
}
