﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class ChangeColor : MonoBehaviour {
public Text MaxScore;
private float timer = 14;
void Update(){
  if (timer > 0)
        {
           timer -= Time.deltaTime;
		   if (timer > 12 && timer < 14) {MaxScore.GetComponent<Text>().color = new Color(0, 255, 0);}
      if (timer > 10 && timer < 12) {MaxScore.GetComponent<Text>().color = new Color(255, 255, 0);}
      if (timer > 8 && timer < 10) {MaxScore.GetComponent<Text>().color = new Color(255, 0, 0);}
      if (timer > 6 && timer < 8) {MaxScore.GetComponent<Text>().color = new Color(255, 0, 162);}
     if (timer > 4 && timer < 6) {MaxScore.GetComponent<Text>().color = new Color(160, 0, 255);}
      if (timer > 2 && timer < 4) {MaxScore.GetComponent<Text>().color = new Color(0, 0, 255);}
      if (timer > 0 && timer < 2) {MaxScore.GetComponent<Text>().color = new Color(0, 255, 255);}
	  }
	else if (timer< 0){timer =+14;}
        }
}
